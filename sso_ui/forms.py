from django.forms import ModelForm, ChoiceField
from .models import Profile, User
from crispy_forms.helper import FormHelper

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['color', 'faculty', 'study_program', 'picture']
        
    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False 

class NameForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name']
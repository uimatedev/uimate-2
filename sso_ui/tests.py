"""SSO UI tests module."""
import json

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django_cas_ng.signals import cas_user_authenticated

from .models import ORG_CODE, Profile


class SSOUITest(TestCase):
    """Test SSO UI app."""

    ATTRIBUTES = {
        "nama": "Ice Bear",
        "peran_user": "mahasiswa",
        "npm": "1706123123",
        "kd_org": "01.00.12.01"
    }

    def setUp(self):
        """Set up test."""
        self.user = User.objects.create_superuser(
            username='username', password='password', email='username@test.com'
        )

    def test_login_url_exists(self):
        """Test if login url exists and redirects to CAS server (response code 302)."""
        response = self.client.get(reverse('sso_ui:loginsso'))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith(settings.CAS_SERVER_URL))

    def test_logout_url_exists(self):
        """Test if logout url exists and redirects to CAS server (response code 302)."""
        response = self.client.get(reverse('sso_ui:logoutsso'))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith(settings.CAS_SERVER_URL))

    def test_profile_url_unauthenticated(self):
        """Test if profile url redirects an unauthenticated user."""
        response = self.client.get(reverse('sso_ui:profile', args=["asd"]))
        self.assertEqual(response.status_code, 404)

    def test_profile_url_post_bad_request(self):
        """Test if profile url redirects an unauthenticated user."""
        response = self.client.post(reverse('sso_ui:profile', args=["asd"]))
        self.assertEqual(response.status_code, 400)

    def test_login_page_200(self):
        response = self.client.get('/login/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Masuk", content)

    def test_signup_page_400(self):
        response = self.client.post('/signup/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 400)

    def test_signup_page_200(self):
        response = self.client.get('/signup/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Daftar", content)

    def test_login_logout_api(self):
        response = self.client.get('/loginapi/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("400", content)
        self.assertEqual(response['content-type'], 'application/json')

    def test_signup_api(self):
        response = self.client.get('/signupapi/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("400", content)
        self.assertEqual(response['content-type'], 'application/json')

    def test_signup_profile_logout_login_logout_api(self):

        response = self.client.post('/signupapi/', data={
            'username': 'hocky', 'password1': "Zd3b75kkQuFvDxq", 'password2': "ZdQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("gagal", content)
        self.assertEqual(response['content-type'], 'application/json')

        old_count = Profile.objects.all().count()
        response = self.client.post('/signupapi/', data={
            'username': 'hocky', 'password1': "Zd3b75kkQuFvDxq", 'password2': "Zd3b75kkQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("berhasil", content)
        self.assertEqual(response['content-type'], 'application/json')

        self.assertEqual(Profile.objects.all().count(), old_count+1)

        response = self.client.get('/profile/hocky/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/signup/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/login/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/logoutsso/')

        response = self.client.post('/login/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 400)

        response = self.client.post('/loginapi/', data={
            'username': 'hoky', 'password': "Zd3b75kkvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("tidak tersedia", content)
        self.assertEqual(response['content-type'], 'application/json')

        response = self.client.post('/loginapi/', data={
            'username': 'hocky', 'password': "Zd3b75kkvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("salah", content)
        self.assertEqual(response['content-type'], 'application/json')

        response = self.client.post('/loginapi/', data={
            'username': 'hocky', 'password': "Zd3b75kkQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("berhasil", content)
        self.assertEqual(response['content-type'], 'application/json')

        response = self.client.get('/profileapi/')
        jsonContent = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonContent['status'], 400)

        response = self.client.get('/nameapi/')
        jsonContent = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonContent['status'], 400)

        response = self.client.post('/profileapi/', data={
            'username': 'hocky', 'password': "Zd3b75kkQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        jsonContent = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonContent['status'], 400)

        response = self.client.post('/nameapi/', data={
            'first_name': 'asdsadasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasd', 'last_name': "asdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdasdasdasdasd"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        jsonContent = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonContent['status'], 400)

        response = self.client.post('/profileapi/', data={
            'color': 'red', "faculty": "Ilmu Komputer", 'study_program': "Ilmu Komputer", "picture": "https://avatarfiles.alphacoders.com/901/thumb-90193.png"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        jsonContent = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonContent['status'], 200)

        response = self.client.post('/nameapi/', data={
            'first_name': 'asdsd', 'last_name': "asdssdasd"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        jsonContent = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonContent['status'], 200)

    def test_profile_can_save_attributes(self):
        """Test if Profile model can save the attributes from CAS."""
        cas_user_authenticated.send(
            sender=self,
            user=self.user,
            created=False,
            attributes=SSOUITest.ATTRIBUTES
        )
        self.assertJSONEqual(
            json.dumps({
                "nama": self.user.get_full_name(),
                "peran_user": self.user.profile.role,
                "npm": self.user.profile.npm,
                "kd_org": self.user.profile.org_code
            }),
            SSOUITest.ATTRIBUTES
        )
        self.assertJSONEqual(
            json.dumps({
                "faculty": self.user.profile.faculty,
                "study_program": self.user.profile.study_program,
                "educational_program": self.user.profile.educational_program
            }),
            ORG_CODE['id'][SSOUITest.ATTRIBUTES['kd_org']]
        )
        self.assertEqual(self.user.email, f"{self.user.username}@ui.ac.id")
        self.assertEqual(self.user.first_name, "Ice")
        self.assertEqual(self.user.last_name, "Bear")

    def test_profile_str(self):
        """Test string representation of Profile model."""
        self.assertEqual(str(self.user.profile),
                         self.user.get_full_name() or "Pengguna Misterius")

    def test_profile_url_show_data_authenticated(self):
        """Test if profile url shows data in the page for an authenticated user."""
        cas_user_authenticated.send(
            sender=self,
            user=self.user,
            created=False,
            attributes=SSOUITest.ATTRIBUTES
        )
        self.client.login(username='username', password='password')
        response = self.client.get(
            reverse('sso_ui:profile', args=["username"]))
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn(self.user.get_full_name(), content)
        self.assertIn(self.user.profile.study_program, content)
        self.assertIn(self.user.profile.faculty, content)

    def test_admin_cant_change_profile(self):
        """Test if admin can't change profile model fields."""
        self.client.login(username='username', password='password')
        response = self.client.get(
            reverse(
                'admin:sso_ui_profile_change',
                kwargs={'object_id': self.user.profile.id}
            )
        )
        content = response.content.decode('utf-8')
        self.assertNotIn('<input type="text"', content)
        self.assertIn('<div class="readonly">', content)

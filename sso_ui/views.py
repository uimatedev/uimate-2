"""SSO UI views module."""
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponseBadRequest
from kenalan.models import Pertemuan
from .models import Profile, User
from .forms import ProfileForm, NameForm
import copy, random

original = {
    "description" : "Kenalan, Yuk!",
    "title" : "Kenalan, Yuk!",
    "need_login" : False
}

# @login_required(login_url='sso_ui:login')
def profile(request,username):
    if(request.method != "GET"):
        return HttpResponseBadRequest()
    profil = get_object_or_404(User, username=username).profile
    context = copy.copy(original)
    context["description"] = f"Profil {username}" 
    context["title"] = username
    context["profile"] = profil
    context["pertemuans"] = profil.peserta.all()
    if(request.user.is_authenticated and username==request.user.username):
        context["name_form"] = NameForm(request.POST or None, instance = request.user)
        context["form"] = ProfileForm(request.POST or None, instance=profil)
    return render(request, "profile.html", context)


def profileapi(request):
    if(request.is_ajax and request.method == "POST"):
        form = ProfileForm(request.POST, instance=request.user.profile)
        if(form.is_valid()):
            form.save()
            return JsonResponse({
            "status":200,
            "message":"Penggantian data profil berhasil ✔️",
            "fakultas":form.cleaned_data["faculty"],
            "jurusan":form.cleaned_data["study_program"],
            "warna":form.cleaned_data["color"],
            "picture":form.cleaned_data["picture"],
            })
        else:
            form = form.__str__()
            return JsonResponse({"status":400, "message":"Penggantian gagal 😵", "htmlform":form})
    return JsonResponse({"status":400, "message":"Server bermasalah 😵"})


def nameapi(request):
    if(request.is_ajax and request.method == "POST"):
        form = NameForm(request.POST, instance=request.user)
        # print(form)
        if(form.is_valid()):
            form.save()
            return JsonResponse({
            "status":200,
            "message":"Penggantian nama berhasil ✔️",
            "nama":form.cleaned_data["first_name"] + " " + form.cleaned_data["last_name"]})
        else:
            form = form.__str__()
            return JsonResponse({"status":400, "message":"Penggantian gagal 😵", "htmlform":form})
    return JsonResponse({"status":400, "message":"Server bermasalah 😵"})


def signupview(request):
    if(request.method != "GET"):
        return HttpResponseBadRequest()

    if(request.user.is_authenticated):
        return redirect('main:home')

    context = copy.copy(original)
    context["description"] = f"Daftar ke UImate"
    context["title"] = f"Daftar ke UImate"
    
    context["form"] = UserCreationForm()

    return render(request, "signup.html", context)

def loginview(request):
    if(request.method != "GET"):
        return HttpResponseBadRequest()

    if(request.user.is_authenticated):
        return redirect('main:home')

    context = copy.copy(original)
    context["description"] = f"Masuk ke UImate"
    context["title"] = f"Masuk ke UImate"
    context["form"] = AuthenticationForm()

    return render(request, "login.html", context)

def loginapi(request):
    if(request.is_ajax and request.method == "POST"):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)
        # print(user)
        # print(type(user))
        if user is not None:
            login(request, user)
            return JsonResponse({"status":200, "message":"Log in berhasil"})
        else:
            try:
                user = User.objects.get(username=username)
                return JsonResponse({"status":400, "message":"Password yang Anda masukkan salah ☹️"})
            except:
                return JsonResponse({"status":400, "message":"Akun dengan username ini tidak tersedia 👎"})
    return JsonResponse({"status":400, "message":"Server bermasalah 😵"})


def signupapi(request):

    if(request.is_ajax and request.method == "POST"):
        # print(request.POST)
        form = UserCreationForm(request.POST)
        if(form.is_valid()):
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
            login(request, new_user)
            return JsonResponse({"status":200, "message":"Pendaftaran berhasil ✔️", "username":form.cleaned_data['username']})
        else:
            # print("Verdict__________________________________")
            form = form.__str__()
            # print(form)
            return JsonResponse({"status":400, "message":"Pendaftaran gagal 😵", "htmlform":form})

    return JsonResponse({"status":400, "message":"Server bermasalah 😵"})

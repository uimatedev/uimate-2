"""SSO UI urls module."""
from django.urls import path
from django_cas_ng import views as cas_views

from . import views

app_name = 'sso_ui'
urlpatterns = [
    path('profile/<str:username>/', views.profile, name='profile'),
    path('login/', views.loginview, name="login"),
    path('signup/', views.signupview, name='signup'),
    path('loginapi/', views.loginapi, name="loginapi"),
    path('signupapi/', views.signupapi, name="signupapi"),
    path('nameapi/', views.nameapi, name="nameapi"),
    path('profileapi/', views.profileapi, name="profileapi"),
    path('loginsso/', cas_views.LoginView.as_view(), name='loginsso'),
    path('logoutsso/', cas_views.LogoutView.as_view(), name='logoutsso'),
]

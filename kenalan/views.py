from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import Http404, JsonResponse
from django.core.serializers.json import DjangoJSONEncoder
from .models import *
from .forms import *
import copy, random
import json
from django.utils import timezone
colors = ["red", "orange", "yellow", "green", "blue", "purple", "violet"]

original = {
    "description" : "Kenalan, Yuk!",
    "title" : "Kenalan, Yuk!",
    "need_login" : False
}

def get_hour(deltaHour):
    now = timezone.now()
    # Rounds to nearest hour by adding a timedelta hour if minute >= 30
    return (now.replace(second=0, microsecond=0, minute=0, hour=now.hour)
               +timedelta(hours=(now.minute//30)+deltaHour))

def get_random_title():
    title = ["Pertemuan seru!", "Main game bareng!", "Cari token!", "Ayo belajar matdas!", "Ayo main Genshin!", "Dota Yuk!!!"]
    return random.choice(title)

def index(request):
    context = copy.copy(original)
    return render(request, "kenalan.html", context)

def reset_pertemuan():
    # Menghapus pertemuan yang sudah lewat 6 jam
    for pertemuan in Pertemuan.objects.all():
        if(pertemuan.waktu_mulai+timedelta(hours=6) < timezone.now()): pertemuan.delete()
        else: break

# def pertemuan_api(request):
#     reset_pertemuan()
#     pertemuans = [dict(items) for items in Pertemuan.objects.all().values(
#         'nama', 'pembuat', 'waktu_mulai', 'durasi')]
#     # print(pertemuans)
#     return JsonResponse({"pertemuans" : pertemuans})

def daftar_pertemuan(request):
    reset_pertemuan()
    context = copy.copy(original)
    context["description"] = "Lihat-lihat ada pertemuan apa aja"
    context["title"] = "Daftar Pertemuan"
    context["pertemuans"] = Pertemuan.objects.all()
    return render(request, "daftar_pertemuan.html", context)

def tambah_pertemuan(request):
    context = copy.copy(original)
    context["description"] = "Tambahkan pertemuan baru di sini"
    context["title"] = "Tambah Pertemuan"
    context["need_login"] = True
    request.POST._mutable = True
    if(request.POST): form = PertemuanForm(request.POST)
    else: form = PertemuanForm(initial={'waktu_mulai': get_hour(1), 'nama':get_random_title()})

    if(not request.user.is_authenticated):
        messages.warning(request, "Login dulu untuk bisa menambahkan pertemuan!")
        if(request.method == "POST"): return redirect('sso_ui:login')
    else:
        form.data["pembuat"] = request.user.profile.id
        form.data["peserta"] = request.user.profile.id
        
    if(form.is_valid() and request.method == "POST"):
        nama = form.cleaned_data['nama']
        messages.success(request, f'Berhasil menambahkan pertemuan {nama}')
        form.save()
        return redirect('kenalan:daftar_pertemuan')

    context["form"] = form
    return render(request, "tambah_pertemuan.html", context)

def hapus_pertemuan(request, id):
    if(request.method != "POST"):
        return redirect('kenalan:daftar_pertemuan')
    
    context = copy.copy(original)
    pertemuan = get_object_or_404(Pertemuan, id=id)

    if(str(pertemuan.pembuat.id) != str(request.user.profile.id)): raise Http404("Non-maker attempt to delete meeting")

    if request.method == 'POST':
        messages.success(request, f'{pertemuan} berhasil dihapus')
        pertemuan.delete()
    
    return redirect('kenalan:daftar_pertemuan')

def detail_pertemuan(request, id):
    context = copy.copy(original)
    all_pertemuan = Pertemuan.objects.all()
    for i in range(len(all_pertemuan)):
        if(all_pertemuan[i].id == id):
            context["pertemuan"] = all_pertemuan[i]
            context["next_pertemuan"] = all_pertemuan[i+1 if i+1 < len(all_pertemuan) else 0].id
            context["prev_pertemuan"] = all_pertemuan[i-1 if i > 0 else len(all_pertemuan)-1].id
            break

    if("pertemuan" not in context): raise Http404("Pertemuan tidak ditemukan!")

    context["title"] = f'{context["pertemuan"].nama}'
    context["description"] = f'Detail pertemuan {context["pertemuan"].nama}'
    context["all_peserta"] = context["pertemuan"].peserta.all()
    context["kapasitas"] = f'{len(context["all_peserta"])}/{context["pertemuan"].kapasitas}'
    context["komentars"] = context["pertemuan"].komentar.all()
    
    if(len(context["all_peserta"]) == 0):
        context["all_peserta"] = ["Hah? Kosong! 🐴"]

    if(request.user.is_authenticated):
        context["full_name"] = (request.user.profile.__str__())
        # print(context["all_peserta"])
        try:
            context["cant_register"] = (request.user.profile.id in [x.id for x in context["all_peserta"]])
        except:
            context["cant_register"] = False
    
    context["komentar_form"] = KomentarForm(request.POST or None)
    return render(request, "detail_pertemuan.html", context)

def toggle_hadir(request):
    if(request.method != "POST"):
        return redirect("kenalan:daftar_pertemuan")
    data = request.POST
    pertemuan = get_object_or_404(Pertemuan, id=data['pertemuan'])
    peserta = request.user.profile
    if(peserta in pertemuan.peserta.all()):
        pertemuan.peserta.remove(peserta)
        messages.info(request, f'Kamu tidak jadi hadir di pertemuan ini!') 
    else:
        if(len(pertemuan.peserta.all()) >= pertemuan.kapasitas):
            messages.error(request, f'Belum bisa menambahkan karena kapasitasnya penuh 😔')
        else:
            pertemuan.peserta.add(peserta)
            messages.success(request, f'Kamu akan hadir di pertemuan ini!') 
    return redirect("kenalan:detail_pertemuan", id=data['pertemuan'])

def tambah_komentar(request, id):
    if(request.method != "POST" or not request.user.is_authenticated or not request.is_ajax):
        return JsonResponse({"status":400, "message":"Server bermasalah 😵"})
    
    request.POST._mutable = True
    komentarForm = KomentarForm(request.POST)
    komentarForm.data["penulis"] = request.user.profile.id
    komentarForm.data["pertemuan"] = id

    if(komentarForm.is_valid()):
        komentarForm.save()
        return JsonResponse({
        "status":200,
        "message":"Komentar berhasil disubmit ✔️"})
        
    komentarForm = komentarForm.__str__()
    # print(komentarForm)
    return JsonResponse({"status":400, "message":"Komentar gagal dipost 😵", "htmlform":komentarForm})

def ambil_komentar(request, id):
    if(request.method != "POST" or not request.user.is_authenticated or not request.is_ajax):
        return JsonResponse({"status":400, "message":"Server bermasalah 😵"})
    
    komentars = get_object_or_404(Pertemuan, id=id).komentar.all()

    # print(request.POST)
    komentars = komentars[int(request.POST["panjang"]):]
    context = {
        'pemilik':[],
        'color':[],
        'nama':[],
        'konten':[],
        'username':[],
    }
    for komentar in komentars:
        penulis = komentar.penulis
        context["nama"].append(penulis.__str__())
        print(penulis.user.username)
        context["username"].append(penulis.user.username)
        context["color"].append(penulis.color)
        context["pemilik"].append("self" if request.user.id == penulis.user.id else "others")
        context["konten"].append(komentar.konten)
        
    return JsonResponse(context)

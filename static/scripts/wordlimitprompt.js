$(document).ready( () => {
    $("#message-input-field").focus( () => {
        $("#word-limit-prompt").animate({width:'toggle'}, 350);
    });

    $("#message-input-field").focusout( () => {
        $("#word-limit-prompt").animate({width:'toggle'}, 350);
    });

    $("#message-input-field").keyup( () => {
        const length = $("#message-input-field").val().length;
        $('#word-limit-prompt span').text(`${length}/100`);
    });
});

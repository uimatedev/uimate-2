$(document).ready( () => {
    $.ajax({
        url: 'randommessage',
        success: (res) => {
            // Refresh the random message
            $("#random-message-div").empty();
            if (res.hasValue) {
                const content = res.data.message;
                const author = res.data.author;
                const element = ` \
                    <p class="random-message">${content}</p> \
                    <b class="random-message">${author}</b>`;
                $("#random-message-div").append(element);
                $("#random-message-div")
                    .css('opacity', 0)
                    .slideDown(500)
                    .animate(
                        { opacity: 1 },
                        { queue: false, duration: 750 }
                    );
            } else {
                const content = "Bukan mager, tapi lagi Power Saving Mode ⚡";
                const author = "Rumput yang Bergoyang 🌿";
                const element = ` \
                    <p class="random-message">${content}</p> \
                    <b class="random-message">${author}</b>`;
                $("#random-message-div").append(element);
                $("#random-message-div")
                    .css('opacity', 0)
                    .slideDown(500)
                    .animate(
                        { opacity: 1 },
                        { queue: false, duration: 750 }
                    );
            }

            // Random the message color
            colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'violet'];
            randomcolor = colors[Math.floor(Math.random() * colors.length)];
            $("#random-message-div").css("color", `var(--main-dark-${randomcolor})`);
        }
    });
});

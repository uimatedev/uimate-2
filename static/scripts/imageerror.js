$(document).ready(function() {
    const imagePreview = document.getElementById("img-preview")
    const imageWarning = document.getElementById("img-warning")
    imagePreview.onerror = function(e) {
        imagePreview.src = "https://media1.tenor.com/images/6a842a184684948163e57d3920784cf2/tenor.gif?itemid=17388148"
        imageWarning.style.display = "block"
    };
});
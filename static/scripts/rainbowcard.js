// from https://www.w3schools.com/howto/howto_css_smooth_scroll.asp
$(document).ready(function(){
  // Add smooth scrolling to all links
  colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'violet'];
  pertemuans = document.getElementsByClassName("rainbow-card")
  for(let i = 0;i < pertemuans.length;i++){
    pertemuans[i].style.backgroundColor = `var(--main-light-${colors[i%7]})`;
    pertemuans[i].style.boxShadow = `5px 5px var(--main-${colors[i%7]})`;
  }
});

from django.test import TestCase, Client
from .models import *
from . import views, models, forms
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User
import requests

# Create your tests here.
BARANG ={
    'nama':'Buku Ajaib Kak Pewe',
    'harga':'Rp.0',
    'lokasi':'DKI JAKARTA',
    'pemilik':'Kak Pewe',
    'kontak':'@kakpeweunyu',
    'gambar': 'https://uploadstatic-sea.mihoyo.com/contentweb/20200616/2020061611214389168.png',
    'deskripsi':'buku biasa yang diberi sihir oleh Kak Pewe secara personal',
}
BARANG_KOSONG ={
    'nama':'',
    'harga':'',
    'lokasi':'',
    'pemilik':'',
    'kontak':'',
    'gambar':'',
    'deskripsi':'',
}
class PassingBarangUnitTest(TestCase):
######################
# url
    def test_list_barang_url_exists(self):
        response = Client().get('/passing/')
        self.assertEqual(response.status_code,200)
    
    def test_detil_barang_url_exists(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/1/')
        self.assertEqual(response.status_code,200)

    def test_tambah_barang_url_exists(self):
        response = Client().get('/passing/tambah/')
        self.assertEqual(response.status_code,200)

    def test_konfirmasi_url_exists(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/confirm/1/')
        self.assertEqual(response.status_code,200)

    def test_status_all_url_exist(self):
        response = Client().get('/passing/status_all/')
        self.assertEqual(response.status_code, 200  )

    def test_area_status_url_exists(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe", lokasi = "DKI JAKARTA")
        new_barang.save()
        response = Client().get('/passing/status/DKI%20JAKARTA/')
        self.assertEqual(response.status_code, 200  )

    def test_status_json_url_exist(self):
        response = Client().get('/passing/status_json/')
        self.assertEqual(response.status_code, 200 )
###################################
# views

    # test list barang
    def test_list_barang_view(self):
        func = resolve('/passing/')
        self.assertEqual(func.func, views.list_barang)

    def test_list_barang_redirection(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/', {'id':1})
        self.assertEqual(response.status_code, 302)

    def test_list_barang_redirec_to_correct_link(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/', {'id':1})
        self.assertRedirects(response, '/passing/detil/1/')

    # test detil barang 
    def test_detil_barang_view(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        func = resolve('/passing/detil/1/')
        self.assertEqual(func.func, views.detil)

    def test_detil_redirection_after_flag(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/detil/1/', {"flag":"flag"})
        url = "/passing/detil/confirm/1/"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, url)

    def test_detil_redirection_status(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe", lokasi = "DKI JAKARTA")
        new_barang.save()
        response = Client().post('/passing/detil/1/', {"status":"status"})
        url = "/passing/status/DKI%20JAKARTA/"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, url)
    
    
    # test tambah barang
    def test_tambah_view(self):
        func = resolve('/passing/tambah/')
        self.assertEqual(func.func, views.tambah)

    def test_tambah_redirection(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertEqual(response.status_code, 302)

    def test_tambah_redirect_to_url(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertRedirects(response, '/passing/')

    def test_tambah_object_count(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertEqual(models.Barang.objects.all().count(), 1)

    def test_barang_form_not_logged_in(self):
        response = Client().post('/passing/tambah/', BARANG)
        self.assertFalse(models.Barang.objects.get(id = 1).pembuat)

    def test_barang_form_logged_in(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username = 'john', password = 'johnpassword')
        response = self.client.post('/passing/tambah/', BARANG)
        self.assertTrue(models.Barang.objects.get(id = 1).pembuat)
    
    
    # test konfirmasi
    def test_confirm_view(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        func = resolve('/passing/detil/confirm/1/')
        self.assertEqual(func.func, views.confirm)

    def test_confirm_deletion(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/detil/confirm/1/', {"delete":"delete"})
        self.assertEqual(models.Barang.objects.all().count(), 0)

    def test_deletion_verified_item(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        response = Client().post('/passing/detil/confirm/1/', {"delete":"delete"})
        self.assertEqual(models.Barang.objects.all().count(), 1)

    def test_confirm_redirection_after_deletion(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().post('/passing/detil/confirm/1/', {"delete":"delete"})
        self.assertRedirects(response, '/passing/')
    
    
    # test area status
    def test_area_status_view(self):
        func =resolve('/passing/status/DKI%20JAKARTA/')
        self.assertEqual(func.func, views.area_status)

    # test all status
    def test_status_all_view(self):
        func =resolve('/passing/status_all/')
        self.assertEqual(func.func, views.status_all)

    # test status json
    def test_status_json_view(self):
        func =resolve('/passing/status_json/')
        self.assertEqual(func.func, views.status_json)
##############################
# templates
    # test list_barang
    def test_list_barang_template(self):
        response = Client().get('/passing/')
        self.assertTemplateUsed(response, 'list_barang.html')

 
    # test detil barang
    def test_detil_barang_template(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/1/')
        self.assertTemplateUsed(response, 'detil_barang.html')
 
    def test_detil_barang_owner(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        self.client.login(username = 'john', password = 'johnpassword')
        response = self.client.get('/passing/detil/1/')
        html = response.content.decode("utf8")
        self.assertIn('Flag as Taken', html)
 
    def test_detil_barang_non_owner(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        User.objects.create_user('gyro', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username = 'gyro', password = 'johnpassword')
        response = self.client.get('/passing/detil/1/')
        html = response.content.decode("utf8")
        self.assertNotIn('Flag as Taken', html)

    def test_detil_not_logged_in(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        response = self.client.get('/passing/detil/1/')
        html = response.content.decode("utf8")
        self.assertNotIn('Flag as Taken', html)
    def test_detil_no_owner(self):
        new_barang = models.Barang(**BARANG)
        new_barang.save()
        response = self.client.get('/passing/detil/1/')
        html = response.content.decode("utf8")
        self.assertIn('Flag as Taken', html)
        self.assertIn('Pemilik barang belum terverifikasi', html)
 
    # test tambah barang
    def test_tambah_barang_template(self):
        response = Client().get('/passing/tambah/')
        self.assertTemplateUsed(response, 'tambah_barang.html')

    def test_tambah_barang_not_logged_in(self):
        response = self.client.get('/passing/tambah/')
        html = response.content.decode("utf8")
        self.assertIn('Anda belum login! Barang anda akan bisa di "flag" oleh siapapun!', html)

    def test_tambah_barang_logged_in(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username = 'john', password = 'johnpassword')
        response = self.client.get('/passing/tambah/')
        html = response.content.decode("utf8")
        self.assertNotIn('Anda belum login! Barang anda akan bisa di "flag" oleh siapapun!', html)
 
    # test konfirmasi
    def test_konfirmasi_template(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        response = Client().get('/passing/detil/confirm/1/')
        self.assertTemplateUsed(response, 'konfirmasi.html')

    def test_konfirmasi_owner(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        self.client.login(username = 'john', password = 'johnpassword')
        response = self.client.get('/passing/detil/confirm/1/')
        html = response.content.decode("utf8")
        self.assertNotIn('mengkontak pemilik', html)

    def test_konfirmasi_non_owner(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        User.objects.create_user('gyro', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username = 'gyro', password = 'johnpassword')
        response = self.client.get('/passing/detil/confirm/1/')
        html = response.content.decode("utf8")
        self.assertIn('Sepertinya anda bukan pemilik barang ini', html)
    
    def test_konfirmasi_not_logged_in(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        new_barang = models.Barang(pembuat = user.profile, **BARANG)
        new_barang.save()
        response = self.client.get('/passing/detil/confirm/1/')
        html = response.content.decode("utf8")
        self.assertIn('Sepertinya anda bukan pemilik barang ini', html)
    
    def test_konfirmasi_no_owner(self):
        new_barang = models.Barang(**BARANG)
        new_barang.save()
        response = self.client.get('/passing/detil/confirm/1/')
        html = response.content.decode("utf8")
        self.assertIn('mengkontak pemilik', html)


    # test area status
    def test_area_status_template(self):
        response = Client().get('/passing/status/DKI%20JAKARTA/')
        self.assertTemplateUsed(response, 'status.html')

    def test_area_status_report(self):
        data = requests.get('https://data.covid19.go.id/public/api/prov.json').json()['list_data']
        request = HttpRequest()
        response = views.area_status(request, "DKI JAKARTA").content.decode('utf8')
        self.assertIn(str(data[0]['jumlah_meninggal']), response)

    # test all status
    def test_all_status(self):
        response = Client().get('/passing/status_all/')
        self.assertTemplateUsed(response, 'status_all.html')

    # test status json
    def test_status_json(self):
        response = Client().get('/passing/status_json/')
        json_file = requests.get('https://data.covid19.go.id/public/api/prov.json').json()
        self.assertEqual(response.json()['DKI JAKARTA']['jumlah_kasus'],json_file['list_data'][0]['jumlah_kasus'] )
#################################
# models
    # test barang model
    def test_barang_model_exist(self):
        new_barang = models.Barang(nama ="Buku Ajaib Kak Pewe")
        new_barang.save()
        self.assertEqual(models.Barang.objects.all().count(), 1)

#####################
# forms
    # test barang form
    def test_barang_form_reject_empty(self):
        form = forms.BarangForm(data =BARANG_KOSONG)
        self.assertFalse(form.is_valid())

    def test_barang_form_accept_test(self):
        form = forms.BarangForm(data =BARANG)
        self.assertTrue(form.is_valid())

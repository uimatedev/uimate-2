from django.shortcuts import render
from django.http import HttpResponse
from .models import Tukaran
from django.http import JsonResponse
import requests


# Create your views here.
def get_formulir(request):
    response ={}
    return render(request,'formulir.html', response)

def get_hasil_formulir(request):
    if request.method == 'POST':
        Tukaran.objects.create(nama=request.POST['nama'], makara=request.POST['makara'], ukuran=request.POST['ukuran'], kontak=request.POST['kontak'])
    jakun =Tukaran.objects.all()
    response ={'jakun':jakun}
    return render(request,'tukerjakun.html', response)

def login(request):
    user = request.user
    if user.is_authenticated:
        return redirect('/formulir/')
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/get_hasil_formulir/')
            
    return render(request, 'tukerjakun/formulir.html')

def logout(request):
    logout(request)
    return redirect('/formulir/')

def search_makara(request):
    response = {}
    return render(request,'makara.html', response)

def status_json(request):
    if request.method == "POST":
        query = request.POST.get('query')
        search = Tukaran.objects.filter(makara = query)
        response = JsonResponse(search, safe = False)
        return response
    else:
        search_makara = Tukaran.objects.all()
        response = JsonResponse({"data":search_makara})
        return response

    





